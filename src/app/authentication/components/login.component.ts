import {Component, HostBinding, OnInit} from '@angular/core';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  template: `
    <div class="flex flex-col items-center justify-center bg-gray-50 py-12 px-4 sm:px-6 lg:px-8">
      <div class="rounded-md bg-red-50 p-4 min-w-full" *ngIf="errors.length>0">
        <div class="flex">
          <div class="flex-shrink-0">
            <svg class="h-5 w-5 text-red-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
              <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z" clip-rule="evenodd" />
            </svg>
          </div>
          <div class="ml-3">
            <h3 class="text-sm leading-5 font-medium text-red-800">
              Errors
            </h3>
            <div class="mt-2 text-sm leading-5 text-red-700">
              <ul class="list-disc pl-5 mb-1">
                <li *ngFor="let error of errors">
                  {{error}}
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div class="">
        <div>
          <h2 class="mt-6 text-center text-3xl leading-9 font-extrabold text-gray-900">
            Sign in
          </h2>
        </div>
        <div class="mt-8">
          <input type="hidden" name="remember" value="true">
          <div class="rounded-md shadow-sm">
            <div>
              <input [(ngModel)]="username" aria-label="Email address" name="email" type="email" required
                     class="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                     placeholder="Username">
            </div>
            <div class="-mt-px">
              <input [(ngModel)]="password" aria-label="Password" name="password" type="password" required
                     class="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                     placeholder="Password">
            </div>
          </div>

          <div class="mt-6">
            <button
              (click)="login()"
              class="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition duration-150 ease-in-out">
          <span class="absolute left-0 inset-y-0 flex items-center pl-3">
            <svg class="h-5 w-5 text-indigo-500 group-hover:text-indigo-400 transition ease-in-out duration-150" fill="currentColor"
                 viewBox="0 0 20 20">
              <path fill-rule="evenodd"
                    d="M5 9V7a5 5 0 0110 0v2a2 2 0 012 2v5a2 2 0 01-2 2H5a2 2 0 01-2-2v-5a2 2 0 012-2zm8-2v2H7V7a3 3 0 016 0z"
                    clip-rule="evenodd"/>
            </svg>
          </span>
              Sign in
            </button>
          </div>
        </div>
      </div>
  `,
  styles: []
})

export class LoginComponent implements OnInit {
  username: string;
  password: string;
  errors: Array<string>;

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit(): void {
    this.errors = [];
  }

  login(): void {
    this.errors = [];
    this.authService.login(this.username, this.password);
    if (this.authService.isAuthenticated === true) {
      this.router.navigate(['/red']);
    }
    this.errors.push(`Credentials not valid`);
  }

  @HostBinding('class')
  // tslint:disable-next-line:typedef
  get themeClass() {
    return `w-9/12`;
  }
}
