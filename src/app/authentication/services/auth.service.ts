import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: any = {
    username: 'renatomoor',
    password: 'Picaro1234'
  };

  isAuthenticated: boolean;

  constructor() {
    this.isAuthenticated = false;
  }

  login(username: string, password: string): boolean {
    if (username === this.user.username && password === this.user.password) {
      this.isAuthenticated = true;
      return true;
    }
    return false;
  }

  logout(): void {
    this.isAuthenticated = false;
  }
}
