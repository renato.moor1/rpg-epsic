import { Injectable } from '@angular/core';
import {FirebaseService} from './firebase.service';
import {Observable} from 'rxjs';
import {IGenericComponent} from '../models/IGenericComponent';

@Injectable({
  providedIn: 'root'
})

export class OrangeService {
  list: Observable<IGenericComponent[]>;
  collection = 'orangeComponents';

  constructor(private db: FirebaseService) {
    this.list = db.getList(this.collection);
  }

  toggle(item): void {
    this.db.update(this.collection, item);
  }

  delete(item): void {
    this.db.delete(this.collection, item);
  }
}
