import {Injectable} from '@angular/core';
import {Character} from '../models/character';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class CharacterService {
  list: Observable<Array<Character>>;
  updateError: string;
  createError: string;
  url: string;

  constructor(private http: HttpClient) {
    this.url = 'https://localhost:5001/characters';
    this.updateError = '';
  }

  updateCharacterList(): void {
    this.list = this.http.get<[Character]>(this.url);
  }

  delete(character: Character): void {
    this.http.delete<Character>(this.url + '/' + character.Id).subscribe(() => {
      this.updateCharacterList();
    });
  }


  add(character: Character): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.http.post<Character>(this.url, character).subscribe(() => {
          this.updateCharacterList();
          this.createError = '';
          resolve(true);
        },
        error => {
          this.createError = error.error;
        });
    });
  }

  update(character: Character): void {
    this.http.post<Character>(` ${this.url}/${character.Id}`, character).subscribe(() => {
        this.updateCharacterList();
      },
      error => {
        this.updateError = error.error;
      });
  }
}
