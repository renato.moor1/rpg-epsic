import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {FirebaseService} from './firebase.service';
import {IGenericComponent} from '../models/IGenericComponent';

@Injectable({
  providedIn: 'root'
})

export class PinkService {
  list: Observable<IGenericComponent[]>;
  collection = 'pinkComponents';

  constructor(private db: FirebaseService) {
    this.list = db.getList(this.collection);
  }

  delete(item): void {
    this.db.delete(this.collection, item);
  }

  add(): void {
    this.db.add(this.collection, {});
  }
}
