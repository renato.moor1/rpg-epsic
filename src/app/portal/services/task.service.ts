import { Injectable } from '@angular/core';
import {Task} from '../models/task';
import {Observable} from 'rxjs';
import {FirebaseService} from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class TaskService {
  list: Observable<Task[]>;
  collection = 'tasks';

  constructor(private db: FirebaseService) {
    this.list = db.getList(this.collection);
  }

  delete(task: Task): void {
    this.db.delete(this.collection, task);
  }

  add(task: Task): void {
    this.db.add(this.collection, task);
  }
}
