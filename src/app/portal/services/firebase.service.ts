import { Injectable } from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
  constructor(private afs: AngularFirestore) {}

  getList(collection: string): any {
    return this.afs.collection(collection).valueChanges();
  }

  delete(collection: string, item: any): void {
    this.afs.collection(collection).doc(item.id).delete();
  }

  add(collection: string, item: any): void {
    const id = Math.random().toString(36).slice(2);
    this.afs.collection(collection).doc(id).set({id, ...item});
  }

  update(collection: string, item: any): void {
    this.afs.collection(collection).doc(item.id).update(item);
  }
}
