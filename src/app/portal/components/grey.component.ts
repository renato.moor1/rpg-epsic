import {Component, OnInit} from '@angular/core';
import {PinkService} from '../services/pink.service';

@Component({
  selector: 'app-grey',
  template: `
    <app-card color="gray" full="true">
      <div class="flex justify-between mb-3">
        <p class="self-center">Grey component</p>
      </div>

      <div class="flex flex-col">
        <app-pink [route]="route" *ngFor="let route of routes"></app-pink>
      </div>

    </app-card>
  `,
})

export class GreyComponent implements OnInit {
  constructor(public pinkService: PinkService) {
  }

  routes: any;

  ngOnInit(): void {
    this.routes = [
      {path: 'blue', name: 'Blue'},
      {path: 'red', name: 'Red'},
      {path: 'characters', name: 'Characters'},
      {path: 'logout', name: 'Logout'}
    ];
  }

  add(): void {
    this.pinkService.add();
  }
}
