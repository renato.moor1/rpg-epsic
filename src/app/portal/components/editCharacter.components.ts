import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Character} from '../models/character';
import {CharacterService} from '../services/character.service';

@Component({
  selector: 'app-edit-character',
  template: `
    <div *ngIf="open" class="fixed z-10 inset-0 overflow-y-auto">
      <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
          <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <span class="hidden sm:inline-block sm:align-middle sm:h-screen"></span>&#8203;
        <div
          class="inline-block align-bottom bg-white rounded-lg px-4 pt-5 pb-4 text-left overflow-hidden shadow-xl
           transform transition-all sm:my-8 sm:align-middle sm:max-w-sm sm:w-full sm:p-6"
          role="dialog" aria-modal="true" aria-labelledby="modal-headline">
          <p class="text-2xl text-gray-600 font-bold text-center">Edit Character</p>
          <app-errors [error]="characterService.updateError"></app-errors>
          <div class="mt-1 relative rounded-md shadow-sm text-gray-700">
            <input [(ngModel)]="character.Name" (keydown.escape)="closeModal()" (keydown.enter)="update()"
                   class="form-input block w-full sm:text-sm sm:leading-5"
                   placeholder="Name">
          </div>
          <div class="mt-1 relative rounded-md shadow-sm text-gray-700">
            <input [(ngModel)]="character.HitPoints" (keydown.escape)="closeModal()" (keydown.enter)="update()"
                   class="form-input block w-full sm:text-sm sm:leading-5"
                   type="number"
                   placeholder="hitPoints">
          </div>
          <div class="mt-1 relative rounded-md shadow-sm text-gray-700">
            <input [(ngModel)]="character.Strength" (keydown.escape)="closeModal()" (keydown.enter)="update()"
                   class="form-input block w-full sm:text-sm sm:leading-5"
                   type="number"
                   placeholder="Strength">
          </div>
          <div class="mt-1 relative rounded-md shadow-sm text-gray-700">
            <input [(ngModel)]="character.Defense" (keydown.escape)="closeModal()" (keydown.enter)="update()"
                   class="form-input block w-full sm:text-sm sm:leading-5"
                   type="number"
                   placeholder="Defense">
          </div>
          <div class="mt-1 relative rounded-md shadow-sm text-gray-700">
            <input [(ngModel)]="character.Intelligence" (keydown.escape)="closeModal()" (keydown.enter)="update()"
                   class="form-input block w-full sm:text-sm sm:leading-5"
                   type="number"
                   placeholder="Intelligence">
          </div>
          <div class="mt-1 relative rounded-md shadow-sm text-gray-700">
            <input [(ngModel)]="character.Class" (keydown.escape)="closeModal()" (keydown.enter)="update()"
                   class="form-input block w-full sm:text-sm sm:leading-5"
                   type="number"
                   placeholder="Class">
          </div>
          <div class="mt-5 sm:mt-6">
        <span class="flex w-full">
          <app-button color="red" text="Cancel" (buttonClick)="closeModal()"></app-button>
          <app-button color="green" text="Update" (buttonClick)="update()"></app-button>
        </span>
          </div>
        </div>
      </div>
    </div>
  `,
})

export class EditCharacterComponent {
  @Input() character: Character;
  @Input() open: boolean;

  @Output() close = new EventEmitter<number>();

  constructor(public characterService: CharacterService) {
  }

  closeModal(): void {
    this.close.emit();
  }

  update(): void {
    this.characterService.update(this.character);
  }
}
