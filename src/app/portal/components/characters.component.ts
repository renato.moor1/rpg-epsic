import {Component, HostBinding, OnInit} from '@angular/core';
import {Character} from '../models/character';
import {CharacterService} from '../services/character.service';

@Component({
  selector: 'app-red',
  template: `
    <app-card color="orange" full="true">
      <div class="flex justify-between mb-2">
        <app-add-character (close)="closeModalAdd()" [open]="openAdd"></app-add-character>
        <p> Characters </p>
        <button (click)="openModalAdd()" class="bg-green-200 p-2 rounded-lg shadow hover:bg-green-300 text-green-500">
          <app-svg-add></app-svg-add>
        </button>
      </div>
      <div class="flex flex-wrap">
        <div *ngFor="let character of characterService.list | async">
          <app-character [character]="character"></app-character>
        </div>
      </div>
    </app-card>
  `,
})

export class CharactersComponent implements OnInit {
  openAdd: boolean;

  constructor(public characterService: CharacterService) {}

  @HostBinding('class')
  // tslint:disable-next-line:typedef
  get themeClass() {
    return `w-9/12`;
  }

  ngOnInit(): void {
    this.characterService.updateCharacterList();
  }

  openModalAdd(): void {
    this.openAdd = true;
  }

  closeModalAdd(): void {
    this.openAdd = false;
  }
}
