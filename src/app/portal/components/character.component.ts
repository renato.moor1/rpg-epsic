import {Component, Input, OnInit} from '@angular/core';
import {Character} from '../models/character';
import {CharacterService} from '../services/character.service';

@Component({
  selector: 'app-character',
  template: `
    <app-card color="purple">
      <app-edit-character [character]="character" (close)="closeModalEdit()" [open]="openEdit"></app-edit-character>
      <div class="flex justify-between mb-2">
        <app-character-image [Strength]="character.Strength"></app-character-image>
        <p class="text-base self-center"> {{character.Name}}</p>
        <div class="self-center">
          <button (click)="deleteCharacter()" class="bg-red-200 text-red-900 p-2 rounded-lg shadow hover:bg-red-300 self-center">
            <app-svg-remove></app-svg-remove>
          </button>
        </div>
      </div>

      <div class="border-t border-gray-200 px-2 py-3 sm:p-0">
        <app-point pointName="Hit Points" [points]="character.HitPoints"></app-point>
        <app-point pointName="Strength" [points]="character.Strength"></app-point>
        <app-point pointName="Defense" [points]="character.Defense"></app-point>
        <app-point pointName="Intelligence" [points]="character.Intelligence"></app-point>
        <app-point pointName="Class" [points]="character.Class"></app-point>
      </div>
      <button (click)="openModalEdit()"
              class="bg-orange-200 text-orange-900 p-2 rounded-lg shadow hover:bg-orange-300 w-full mt-2 hover:shadow-lg">
        <app-svg-edit></app-svg-edit>
      </button>
    </app-card>
  `,
})
export class CharacterComponent implements OnInit {
  @Input() character: Character;
  openEdit: boolean;

  constructor(private characterService: CharacterService) {
  }

  ngOnInit(): void {
  }

  openModalEdit(): void {
    this.openEdit = true;
  }

  closeModalEdit(): void {
    this.openEdit = false;
    this.characterService.updateCharacterList();
  }

  deleteCharacter(): void {
    if (confirm('Are you sure to delete ' + this.character.Name)) {
      this.characterService.delete(this.character);
    }
  }
}
