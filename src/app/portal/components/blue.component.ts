import {Component, HostBinding} from '@angular/core';
import {OrangeService} from '../services/orange.service';

@Component({
  selector: 'app-blue',
  template: `
    <app-card color="blue" full="true">
      <p>Blue component</p>
      <div *ngFor="let component of orangeComponents.list | async">
        <app-orange [orangeComponent]="component"></app-orange>
      </div>
    </app-card>
  `,
})
export class BlueComponent {
  constructor(public orangeComponents: OrangeService) {}
  @HostBinding('class')
  // tslint:disable-next-line:typedef
  get themeClass() {
    return `w-9/12`;
  }
}
