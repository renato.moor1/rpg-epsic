import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-point',
  template: `
    <dl class="sm:divide-y sm:divide-gray-200">
      <div class="py-2 sm:grid sm:grid-cols-4 sm:gap-1">
        <dt></dt>
        <dt class="text-sm text-left font-medium text-gray-500">
          {{pointName}}
        </dt>
        <dd class="mt-1 ml-4 text-sm text-gray-900 sm:mt-0 sm:col-span-2 text-left">
          {{points}}
        </dd>
        <dt></dt>
      </div>
    </dl>
  `,
})
export class PointComponent {
  @Input() pointName: string;
  @Input() points: number;

  constructor() {
  }

}
