import {Component, HostBinding, Input} from '@angular/core';
import {AuthService} from '../../authentication/services/auth.service';

@Component({
  selector: 'app-pink',
  template: `
    <a class="bg-gray-400 border shadow rounded w-full py-2"
       routerLinkActive="bg-gray-600 text-gray-200"
       *ngIf="show()"
       routerLink="{{route.path}}">
      {{ route.name }}
    </a>
  `,
})
export class PinkComponent {
  @Input() route: any;
  isActive: boolean;

  constructor(private authService: AuthService) {
    this.isActive = false;
  }

  @HostBinding('class')
  // tslint:disable-next-line:typedef
  get themeClass() {
    return `flex flex-col m-1`;
  }

  show(): boolean {
    return !(this.route.name === 'Logout' && !this.authService.isAuthenticated);
  }
}
