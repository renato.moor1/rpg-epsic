import { Component } from '@angular/core';

@Component({
  selector: 'app-main',
  template: `
    <div class="py-4">
      <app-green></app-green>
      <div class="flex h-64 flex-wrap">
        <app-grey class="w-3/12"></app-grey>
        <router-outlet></router-outlet>
      </div>
    </div>
  `,
})
export class MainComponent {}
