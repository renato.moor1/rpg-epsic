import {Component, HostBinding, OnInit} from '@angular/core';
import {Task} from '../models/task';
import {TaskService} from '../services/task.service';

@Component({
  selector: 'app-red',
  template: `
    <app-card color="red" full="true">
      <div class="flex justify-between mb-2">
        <app-modal (addTask)="addTask($event)" (close)="closeModal()" [open]="open"></app-modal>
        <p> Red component </p>
        <button (click)="openModal()" class="bg-green-200 p-2 rounded-lg shadow hover:bg-green-300 text-green-500">
          <app-svg-add></app-svg-add>
        </button>
      </div>
      <div class="flex flex-wrap">
        <div *ngFor="let task of tasks.list | async">
          <app-task [task]="task"></app-task>
        </div>
      </div>
    </app-card>
  `,
})

export class RedComponent implements OnInit {
  open: boolean;

  constructor(public tasks: TaskService) {}

  @HostBinding('class')
  // tslint:disable-next-line:typedef
  get themeClass() {
    return `w-9/12`;
  }

  ngOnInit(): void {
  }

  openModal(): void {
    this.open = true;
  }

  addTask(task: Task): void {
    this.tasks.add(task);
    this.closeModal();
  }

  closeModal(): void {
    this.open = false;
  }
}
