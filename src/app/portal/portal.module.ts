import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PortalRoutingModule} from './portal-routing.module';
import {BlueComponent} from './components/blue.component';
import {RedComponent} from './components/red.component';
import {OrangeComponent} from './components/orange.component';
import {PinkComponent} from './components/pink.component';
import {TaskComponent} from './components/task.component';
import {ButtonComponent} from './components/button.component';
import {CardComponent} from './components/card.component';
import {ModalComponent} from './components/modal.component';
import {AddComponent} from './components/add.component';
import {RemoveComponent} from './components/remove.component';
import {GreyComponent} from './components/grey.component';
import {GreenComponent} from './components/green.component';
import {MainComponent} from './components/main.component';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../../environments/environment';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {CharactersComponent} from './components/characters.component';
import {AddCharacterComponent} from './components/addCharacter.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {CharacterComponent} from './components/character.component';
import {PointComponent} from './components/point.component';
import {CharacterImageComponent} from './components/characterImage.component';
import {EditCharacterComponent} from './components/editCharacter.components';
import {EditComponent} from './components/edit.component';
import {ErrorsComponent} from './components/errors.component';


@NgModule({
  declarations: [
    MainComponent,
    GreyComponent,
    GreenComponent,
    BlueComponent,
    RedComponent,
    OrangeComponent,
    PinkComponent,
    RedComponent,
    TaskComponent,
    ButtonComponent,
    CardComponent,
    ModalComponent,
    AddComponent,
    EditComponent,
    ErrorsComponent,
    RemoveComponent,
    CharacterImageComponent,
    CharactersComponent,
    AddCharacterComponent,
    EditCharacterComponent,
    CharacterComponent,
    PointComponent,
  ],
  exports: [
    MainComponent
  ],
  imports: [
    CommonModule,
    PortalRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    FormsModule,
    HttpClientModule,
  ]
})
export class PortalModule { }
