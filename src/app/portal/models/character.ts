export interface Character {
  Id: number;
  Name: string;
  HitPoints: number;
  Strength: number;
  Defense: number;
  Intelligence: number;
  Class: number;
}
