import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BlueComponent} from './components/blue.component';
import {RedComponent} from './components/red.component';
import {AuthGuard} from '../authentication/guards/auth.guard';
import {CharactersComponent} from './components/characters.component';

export const routes: Routes = [
  {path: 'blue', component: BlueComponent, canActivate: [AuthGuard]},
  {path: 'red', component: RedComponent, canActivate: [AuthGuard]},
  {path: 'characters', component: CharactersComponent, canActivate: [AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class PortalRoutingModule {
}
