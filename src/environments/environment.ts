// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBZfWolPTo39Bo_KWZttQh39SGJ6f82jt4',
    authDomain: 'angular-d182d.firebaseapp.com',
    databaseURL: 'https://angular-d182d.firebaseio.com',
    projectId: 'angular-d182d',
    storageBucket: 'angular-d182d.appspot.com',
    messagingSenderId: '742341529689',
    appId: '1:742341529689:web:fce37a1920aafedf9f6148'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
